package nz.uoa.rtie478.statuswatcher;

import android.content.Context;
import android.util.Log;

import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;

/**
 * Created by subserver on 20/09/17.
 */
enum State {
    PHONE_CALL ("In Phone Call"),
    IDLE ("Online"),
    MISSED_CALL ("Away");

    private String status;
    State(String status){
        this.status = status;
    }

    public String getStatus(){
        return this.status;
    }
}

public class CallReceiver extends PhoneCallReciever {

    @Override
    protected void onIncomingCallAnswered(Context ctx, String number, Date start)
    {
        sendStatus(State.PHONE_CALL);
    }

    @Override
    protected void onIncomingCallEnded(Context ctx, String number, Date start, Date end)
    {
        sendStatus(State.IDLE);
    }

    @Override
    protected void onOutgoingCallStarted(Context ctx, String number, Date start)
    {
        sendStatus(State.PHONE_CALL);
    }

    @Override
    protected void onOutgoingCallEnded(Context ctx, String number, Date start, Date end)
    {
        sendStatus(State.IDLE);
    }

    @Override
    protected void onMissedCall(Context ctx, String number, Date start)
    {
        sendStatus(State.MISSED_CALL);
    }

    public void sendStatus(State currentState){
        Log.i("Status Update", currentState.getStatus()); //Log the status to logcat.
        new Thread(new StatusSender(currentState.getStatus())).start(); //Send this new status to the server.
    }



}
