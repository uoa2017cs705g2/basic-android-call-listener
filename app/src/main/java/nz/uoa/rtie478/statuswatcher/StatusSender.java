package nz.uoa.rtie478.statuswatcher;

import android.util.Log;

import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by subserver on 20/09/17.
 */

public class StatusSender implements Runnable {

    String status;
    StatusSender(String s) { status = s; }

    @Override
    public void run() {
            try {
                String query = "status="+status;

                URL url = new URL("http://192.168.178.65:80");
                HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                //Set to POST
                connection.setDoOutput(true);
                connection.setRequestMethod("POST");
                connection.setReadTimeout(10000);

                Writer writer = new OutputStreamWriter(connection.getOutputStream());
                writer.write(query);
                writer.flush();
                writer.close();
                

                Log.i("StatusSender", "Request Sent");
            } catch (Exception e) {
                // TODO Auto-generated catch block
                Log.e("StatusSender", e.toString());
            }
    }

}