# Status Gauage Android Monitor
Android application that monitors the phone_state. This app listens to boradcast reciever events such that it can notify the Status Gauge server when a user enters or exits a call on their cell phone.

Uses a HTTP POST request to send the new status information to the Status Gauage server.

# Install & Run
1. Clone project to local directory
2. Open project using Android Studio

# Credits
- Jack Haller <@aucklanduni.ac.nz>
- Ryan Tiedemann <rtie478@aucklanduni.ac.nz>
- Jay Pandya <@aucklanduni.ac.nz>
- Priyankit Singh <@aucklanduni.ac.nz>